import { Configuration } from 'webpack';
import webpackConfig from './webpack.config';

process.env.NODE_ENV = 'production';

const config: Configuration = {
	...webpackConfig,
	mode: 'production',
	devtool: 'source-map',
};

export default config;
