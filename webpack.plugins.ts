import webpack from 'webpack';
import validateOptions from 'schema-utils';
import fs from 'fs-extra';
import path from 'path';

import config from './foundryconfig.json';

class CopyToUserDataPlugin {
	options: any;

	constructor(options: { manifest: string }) {
		const schema: any = {
			type: 'object',
			properties: {
				manifest: {
					description: 'Path to manifest',
					type: 'string',
				},
			},
			additionalProperties: false,
			required: ['manifest'],
		};

		validateOptions(schema, options, {
			name: 'Copy To UserData Plugin',
			baseDataPath: 'data',
		});

		this.options = options || {};
	}

	apply(compiler: webpack.Compiler) {
		const PLUGIN_NAME = 'copy-to-userdata-plugin';
		const logger = compiler.getInfrastructureLogger(PLUGIN_NAME);

		compiler.hooks.done.tap(PLUGIN_NAME, () => {
			logger.debug('copying files to user data after compilation is done');

			const manifest = fs.readJSONSync(this.options.manifest);
			const name = manifest.name;

			let destDir;
			try {
				if (
					fs.existsSync(path.resolve('.', 'dist', 'module.json')) ||
					fs.existsSync(path.resolve('.', 'src', 'module.json'))
				) {
					destDir = 'modules';
				} else if (
					fs.existsSync(path.resolve('.', 'dist', 'system.json')) ||
					fs.existsSync(path.resolve('.', 'src', 'system.json'))
				) {
					destDir = 'systems';
				} else {
					logger.error(`Could not find 'module.json' or 'system.json'`);
					return;
				}

				let linkDir;
				if (path.resolve(config.dataPath)) {
					if (!fs.existsSync(path.join(config.dataPath, 'Data'))) {
						logger.error(
							'User Data path invalid, no Data directory found ' +
								path.resolve(config.dataPath)
						);
						return;
					}

					linkDir = path.join(
						path.resolve(config.dataPath),
						'Data',
						destDir,
						name
					);

					if (!fs.existsSync(linkDir)) {
						logger.info('Symlink build to ' + linkDir);

						fs.symlinkSync(path.resolve('./dist'), linkDir, 'dir');
					} else {
						logger.info('Skipping symlink because it already exists');
					}
				} else {
					logger.error('No User Data path defined in foundryconfig.json');

					return;
				}
			} catch (err) {
				logger.error(err);

				return;
			}

			logger.debug('completed copying build');
		});
	}
}

export { CopyToUserDataPlugin };
