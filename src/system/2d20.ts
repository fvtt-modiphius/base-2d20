export interface RollOptions {
	targetNumber: number;
	extraDice: number;
	difficulty?: number;
	doubleRange?: number;
	complicationRange?: number;
}

export interface RollResult {
	/** The set of d20 rolls */
	results: number[];
	/** The total number of generated Successes */
	successes: number;
	/** The total number of generated Complications */
	complications: number;
	/** Whether the roll succeeded */
	succeeded: boolean;
	/** The total number of generated Momentum */
	momentum: number;
}

/**
 * Performs a standard 2d20 roll.
 *
 * @param targetNumber		Value to compare each d20 against.
 * @param extraDice			How many extra d20s to add to the roll (max 3).
 * @param difficulty		How many successes required to succeed the roll (default 1).
 * @param doubleRange		Results equal to or lower that give double Successes (default 1).
 * @param complicationRange	Range of values to generate Complications (default 1 == 20).
 *
 * @returns					An object containing the generated results.
 */
export function roll2d20({
	targetNumber,
	extraDice,
	difficulty = 1,
	doubleRange = 1,
	complicationRange = 1,
}: RollOptions): RollResult {
	// Execute the standard Xd20 roll.
	const totalDice = 2 + extraDice;
	const roll = new Die({ faces: 20, number: totalDice });
	roll.evaluate();

	// Extract roll results.
	const results = roll.results.map((die) => {
		return die.result as number;
	});

	// Count Successes.
	const successes = results.reduce((total, result) => {
		// Standard success where the result is equal to or lower than the TN.
		if (result <= targetNumber) total++;

		// If the result is also below the range for double Successes (default 1),
		// we add an additional success to the total.
		if (result <= doubleRange) total++;

		return total;
	}, 0);

	// Determine the minimum value that causes a Complication.
	// We subtract from 21 because a range of 1 equals 20.
	const minComplicationValue = 21 - complicationRange;

	// Count Complications.
	const complications = results.reduce((total, result) => {
		// If the result is equal to or higher than the minimum value
		if (result >= minComplicationValue) total++;
		return total;
	}, 0);

	// Roll succeeds if the # of Successes is equal to or higher than the Difficulty.
	const succeeded = successes >= difficulty;

	// Calculate how much Momentum has been generated, which is Successes minus Difficulty,
	// or zero if the total is negative.
	const momentum = Math.max(successes - difficulty, 0);

	return {
		results,
		successes,
		complications,
		succeeded,
		momentum,
	};
}
