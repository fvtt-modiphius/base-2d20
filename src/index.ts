// ============================================================================
// Base 2d20 System - v1.0.0
// Copyright:	The 2d20 system is copyright Modiphius Entertainment Ltd 2020.
//				All 2d20 system text is copyright Modiphius Entertainment Ltd.
// License:		MIT
// ============================================================================

import { roll2d20, RollOptions } from './system/2d20';
import { EffectDie } from './system/EffectDie';

Hooks.on('init', () => {
	console.log('2d20 | Initializing 2d20 module');

	CONFIG.Dice.types.push(EffectDie);
	CONFIG.Dice.terms.e = EffectDie;

	loadTemplates(['modules/base-2d20/templates/2d20-chat-card.html']);
});

Hooks.on('chatMessage', (log, message: string, data) => {
	const is2d20 = message.startsWith('/2d20');

	if (!is2d20) return true;

	const [
		command,
		targetNumber,
		difficulty,
		extraDice,
		doubleRange,
		complicationRange,
	] = message.split(' ');

	const rollOptions: RollOptions = {
		targetNumber: parseInt(targetNumber),
		extraDice: parseInt(extraDice) || 0,
		difficulty: parseInt(difficulty) || 1,
		doubleRange: parseInt(doubleRange) || 1,
		complicationRange: parseInt(complicationRange) || 1,
	};

	const result = roll2d20(rollOptions);

	const chatData = {
		...rollOptions,
		...result,
	};

	renderTemplate('modules/base-2d20/templates/2d20-chat-card.html', chatData).then(
		(html) => {
			const messageData = {
				user: game.user._id,
				type: CONST.CHAT_MESSAGE_TYPES.ROLL,
				content: html,
			};

			console.log(html);

			const rollMode = game.settings.get('core', 'rollMode');
			ChatMessage.applyRollMode(messageData, rollMode);

			CONFIG.ChatMessage.entityClass.create(messageData);
		}
	);

	return false;
});
