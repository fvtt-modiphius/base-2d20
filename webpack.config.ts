import path from 'path';
import fs from 'fs-extra';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import { CopyToUserDataPlugin } from './webpack.plugins';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import TerserPlugin from 'terser-webpack-plugin';

/**
 * @type {webpack.Configuration}
 */
export default {
	entry: {
		'base-2d20': './src/index.ts',
	},
	output: {
		path: path.join(process.cwd(), 'dist'),
		publicPath: '',
		filename: '[name].js',
	},
	module: {
		rules: [
			{
				test: /\.ts$/,
				exclude: /node_modules/,
				use: ['ts-loader'],
			},
			{
				test: /\.scss$/,
				use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
			},
			{
				test: /\.ttf$/,
				loader: 'file-loader',
				options: {
					name: '[folder]/[name].[ext]',
				},
			},
		],
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: '[name].css',
		}),
		new CopyWebpackPlugin({
			patterns: [
				{ from: '**/*.json', context: './src' },
				{ from: '**/*.html', context: './src' },
			],
		}),
		new CopyToUserDataPlugin({ manifest: './src/module.json' }),
	],
	resolve: {
		extensions: ['.js', '.ts', '.json', '.vue'],
		modules: ['node_modules'],
	},
	optimization: {
		minimizer: [
			new TerserPlugin({
				parallel: true,
				terserOptions: {
					compress: true,
					ecma: 2018,
					mangle: false,
				},
			}),
		],
	},
};
