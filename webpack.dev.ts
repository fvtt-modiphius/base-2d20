import { Configuration } from 'webpack';
import webpackConfig from './webpack.config';

const config: Configuration = {
	...webpackConfig,
	mode: 'development',
	devtool: 'source-map',
	watch: true,
	watchOptions: {
		ignored: ['**/*.d.ts', 'node_modules/**'],
	},
};

export default config;
